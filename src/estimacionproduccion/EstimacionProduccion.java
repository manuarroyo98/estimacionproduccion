/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estimacionproduccion;

import Objetos.PDC;
import Objetos.PerdidasCableado;
import Objetos.PerformanceRatio;
import Objetos.PotEntradaInversor;
import Objetos.PotEntregadaRed;
import Objetos.PotMaxPotencia;
import Objetos.PotSalidaInversor;
import Objetos.TemperaturaCelula;
import Objetos.VentaEnergia;
import java.util.ArrayList;

/**
 *
 * @author Manuel Arroyo Setien
 */
public class EstimacionProduccion {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TemperaturaCelula miCelula = new TemperaturaCelula("Valladolid", 35, 42);
        PotMaxPotencia miPotencia = new PotMaxPotencia(miCelula, 108.225);
        PDC miPdc = new PDC(miPotencia);
        PotEntradaInversor miPE = new PotEntradaInversor(miPdc, 3);
        PotSalidaInversor miPS = new PotSalidaInversor(miPE);
        PerdidasCableado miPC = new PerdidasCableado(miPS, 0.9);
        PotEntregadaRed miPER = new PotEntregadaRed(miPC, 1, 3);
        PerformanceRatio miPR = new PerformanceRatio(108.225, miPER, miCelula);
        System.out.println("PERFORMANCE RATIO");
        for (int i=0; i<miPR.getValoresPR().size(); i++) {
            System.out.println(miPR.getValoresPR().get(i) + "%");
        }
        System.out.println("-------------------------------");
        System.out.println("-------------------------------");
        System.out.println("VENTA DE ENERGIA");
        VentaEnergia ve = new VentaEnergia(miCelula, 0.44038, 108.225, miPR);
        for (int i=0; i<ve.getListaPrecios().size(); i++)
            System.out.println(ve.getListaPrecios().get(i) + "€");
    }
    
}
