package Objetos;

import java.util.ArrayList;
/**
 *
 * @author Adrián Sánchez
 */

//Clase para el objeto Potencia de maxima potencia del generador
public class PotMaxPotencia 
{
    /**
     * CONSTANTES
     */    
    //Irradiancia en STC 
    private final double irradiancia = 1;
    //Coeficiente de variacion de la temperatura con la potencia
    private final double coefVariacionTemp = 0.00441;
    //Temperatura en condiciones estandar
    private final double tempEstandar = 25.00;
    //Potencia nominal en STC
    private double potNominal;
    //Array para almacenar el valor de G
    private ArrayList<Double> G;
    //Temperatura celula 
    private TemperaturaCelula tc;
    //Array de valores finales de la potencia de maxima potencia del generador
    private ArrayList<Double> valoresPM;

    public PotMaxPotencia(TemperaturaCelula tc,double potInstalada) 
    {   
        this.tc = tc;
        this.G = (ArrayList<Double>) Calculos.calculaG(tc.getProvincia(),tc.getInclinacion(),tc.getLatitud());
        this.valoresPM = new ArrayList<>();       
        this.potNominal = potInstalada * 1000;
        double valor;
        
        //Bucle para asignar los valores finales
        for(int i=0; i<12; i++)
        {
            //Añado los valores siguiendo la formula para obtener la potencia de maxima potencia del generador
            valor = (potNominal* (this.G.get(i)/irradiancia))*((1-(coefVariacionTemp*(tc.getValoresTC().get(i)-tempEstandar))))/1000;
            valoresPM.add(valor);
        }
    }

    public TemperaturaCelula getTc() {
        return tc;
    }

    public void setTc(TemperaturaCelula tc) {
        this.tc = tc;
    }

    public ArrayList<Double> getValoresPM() {
        return valoresPM;
    }

    public void setValoresPM(ArrayList<Double> valoresPM) {
        this.valoresPM = valoresPM;
    }    

    public ArrayList<Double> getG() {
        return G;
    }

    public void setG(ArrayList<Double> G) {
        this.G = G;
    }
    
}
