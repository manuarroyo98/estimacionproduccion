/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Manuel Arroyo Setien
 */
public class PerdidasCableado {
    //Clase que calcula las perdidas en el cableado, necesita los datos de las PotSalidaInversor y una variable de perdidas Lohm
    private List<Double> listaPerdidas;
    private double perdidas;
    
    public PerdidasCableado(PotSalidaInversor psi, double perd) {
        listaPerdidas = new ArrayList<>();
        this.perdidas = perd;
        rellenarLista(psi);
    }

    public List<Double> getListaPerdidas() {
        return listaPerdidas;
    }

    public void setListaPerdidas(List<Double> listaPerdidas) {
        this.listaPerdidas = listaPerdidas;
    }

    public double getPerdidas() {
        return perdidas;
    }

    public void setPerdidas(double perdidas) {
        this.perdidas = perdidas;
    }
    
    private void rellenarLista(PotSalidaInversor psi) {
        double valor;
        for (int i=0; i<psi.getValoresPotSalida().size(); i++) {
            valor = psi.getValoresPotSalida().get(i) * (1-Calculos.perdidasLohm(perdidas));
            this.listaPerdidas.add(valor);
        }
            
    }
}
