/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Manuel Arroyo Setien
 */
public class TemperaturaCelula {
    private double tempAmbiente, G;
    private final double TONC = 46.00;
    private String provincia;
    private double inclinacion, latitud;
    private ArrayList<Double> valoresTC;
    
    //Se piden en el constructor los valores necesarios para calcular la Temperatura ambiente y la G(KWh/m2 * mes)
    public TemperaturaCelula(String provincia, double inclinacion, double latitud) {
        this.provincia = provincia;
        this.inclinacion = inclinacion;
        this.latitud = latitud;
        //En este método se rellena la lista con los valores de las temperaturas
        rellenarListaValores();
        //Tras estos métodos las variables necesarias para el calculo de la TC tienen un valor
    }

    public double getTempAmbiente() {
        return tempAmbiente;
    }

    public void setTempAmbiente(double tempAmbiente) {
        this.tempAmbiente = tempAmbiente;
    }

    public double getG() {
        return G;
    }

    public void setG(double G) {
        this.G = G;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public double getInclinacion() {
        return inclinacion;
    }

    public void setInclinacion(double inclinacion) {
        this.inclinacion = inclinacion;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public ArrayList<Double> getValoresTC() {
        return valoresTC;
    }

    public void setValoresTC(ArrayList<Double> valoresTC) {
        this.valoresTC = valoresTC;
    }
    
    private void rellenarListaValores() {
        //Para calcular las temperaturas se necesitan las G y las temperaturas de cada mes
        //Temperatura de celula = TempAmbiente + ((TONC-20)/800) * G
        List<Double> listaGs = Calculos.calculaG(this.provincia, this.inclinacion, this.latitud);
        List<Double> listaTemps = Calculos.calculaTemp(this.provincia);
        
        this.valoresTC = new ArrayList<>();
        for (int i=0; i<12; i++) {
                //Calculos por separado de la operacion para calcular cada temperatura
                double temp = listaTemps.get(i);
                double t = (TONC-20)/800;
                double tempt = t*listaGs.get(i);
                double valor = tempt + temp;
                this.valoresTC.add(valor);
            }
            //Con esto la lista de valores ya contiene las temperaturas de celulas de cada mes
        }
    }
    
