/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Manuel Arroyo Setien
 */
public class VentaEnergia {
    private double precio, prima, potInstalada;
    private List<Double> listaPrecios;
    
    public VentaEnergia (TemperaturaCelula tc, double prima, double potenciaInstalada, PerformanceRatio pr) {
        this.listaPrecios = new ArrayList<>();
        this.potInstalada = potenciaInstalada;
        this.prima = prima;
        List<Double> listaGs = Calculos.calculaG(tc.getProvincia(), tc.getInclinacion(), tc.getLatitud());
        //La formula de la venta de energia es diasMes * radiacion * PerformanceRatio * potenciaInstalada * Prima
        //Para calcular las radiaciones de cada mes es G/dias del mes
        List<Double> radiaciones = Calculos.calculoRadiacionesVentasEnergia(listaGs);
        List<Double> producciones = Calculos.calculoProduccionVentasEnergia(radiaciones, pr.getValoresPR(), potInstalada);
        double valor;
        for (int i=0; i<producciones.size(); i++) {
            valor = prima*producciones.get(i);
            listaPrecios.add(valor);
        }
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getPrima() {
        return prima;
    }

    public void setPrima(double prima) {
        this.prima = prima;
    }

    public double getPotInstalada() {
        return potInstalada;
    }

    public void setPotInstalada(double potInstalada) {
        this.potInstalada = potInstalada;
    }

    public List<Double> getListaPrecios() {
        return listaPrecios;
    }

    public void setListaPrecios(List<Double> listaPrecios) {
        this.listaPrecios = listaPrecios;
    }
    
    
}
