/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Manuel Arroyo Setien
 */
public class PotEntradaInversor {
    //Esta clase indica la potencia disponible a la entrada del inversor
    //Su formula de calculo es PDC * (1-perdidas LSPMP)
    //Necesario una por cada mes
    
    private List<Double> potenciasEntrada;
    private double perdidas;
    
    public PotEntradaInversor(PDC pdcs, double per) {
        potenciasEntrada = new ArrayList<>();
        this.perdidas = per;
        rellenarLista(pdcs);
    }

    public List<Double> getPotenciasEntrada() {
        return potenciasEntrada;
    }

    public void setPotenciasEntrada(List<Double> potenciasEntrada) {
        this.potenciasEntrada = potenciasEntrada;
    }

    public double getPerdidas() {
        return perdidas;
    }

    public void setPerdidas(double perdidas) {
        this.perdidas = perdidas;
    }
    
    private void rellenarLista(PDC p) {
        double valor;
        for (int i=0; i<p.getValoresPDC().size(); i++) {
            valor = p.getValoresPDC().get(i) * (1-Calculos.perdidasLSPMP(perdidas));
            this.potenciasEntrada.add(valor);
        }
    }
}
