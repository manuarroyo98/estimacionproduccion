package Objetos;

import java.util.ArrayList;

/**
 *
 * @author Adrián Sánchez
 */
//Clase para el objeto PerfomanceRatio que calcula la eficiencia de la poduccion de energia de la instalacion
public class PerformanceRatio 
{
    //Variable para almacenar la potencia instalada necesario para obtener el performance ratio
    private double potInstalada;
    //Objeto de la potencia entregada a la red utilizado para obtener sus valores
    private PotEntregadaRed potEntrgadaRed;
    //Objeto para obtener la latitud, longuitud y inclinacion para obtener G
    private TemperaturaCelula tc;
    //Array para almacenar los valores finales del performance ratio
    private ArrayList<Double> valoresPR;

    public PerformanceRatio(double potInstalada, PotEntregadaRed potEntrgadaRed, TemperaturaCelula tc) {
        this.potInstalada = potInstalada;
        this.potEntrgadaRed = potEntrgadaRed;
        this.tc = tc;
        this.valoresPR = new ArrayList<>();
        double valor;
        for(int i = 0; i<12; i++)
        {
            //Añado los valores al array siguiendo la formula
            valor = ((this.potEntrgadaRed.getPotenciasRed().get(i)/this.potInstalada) / (Calculos.calculaG(tc.getProvincia(),tc.getInclinacion(),tc.getLatitud()).get(i)))*100 ;
            this.valoresPR.add(valor);
        }
    }

    public double getPotInstalada() {
        return potInstalada;
    }

    public void setPotInstalada(double potInstalada) {
        this.potInstalada = potInstalada;
    }

    public PotEntregadaRed getPotEntrgadaRed() {
        return potEntrgadaRed;
    }

    public void setPotEntrgadaRed(PotEntregadaRed potEntrgadaRed) {
        this.potEntrgadaRed = potEntrgadaRed;
    }

    public TemperaturaCelula getTc() {
        return tc;
    }

    public void setTc(TemperaturaCelula tc) {
        this.tc = tc;
    }

    public ArrayList<Double> getValoresPR() {
        return valoresPR;
    }

    public void setValoresPR(ArrayList<Double> valoresPR) {
        this.valoresPR = valoresPR;
    }
    
    
    
}
