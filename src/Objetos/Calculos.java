/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Manuel Arroyo Setien
 */
public class Calculos {

    public Calculos() {

    }

    public static List<Double> calculaG(String prov, double inc, double lat) {
        List<Double> GMeses = null;
        //try {
        //Para ello se calcula la radiacion en plano horizontal * factor de correccion en funcion de la inclinacion recibida
        //Estos dos datos deben estar en la base de datos para poder realizar el calculo
        //La formula de la G es la radiacion en la inclinacion recibida * numero de dias del mes
        //Luego se divide la cifra entre la constante 3,6
        //G = (radiacion horizontal * factor de correccion * diasMes)/3.6

        //1.- Consultar en base de datos la radiacion en plano horizontal para todos los meses en esa provincia
        //Se hace un array de 12 o una lista con esos valores, ordenados por mes
        //2.- Consultar en base de datos el factor de correccion (k) para todos los meses, esa latitud y esa inclinacion
        //Se hace un array de 12 o una lista con esos valores, ordenados por mes
        //3.- Multiplicar la radiacion en horizontal * factor de correccion (k) * dias del mes
        //Se recorren ambas listas y se multiplica posicion 0 * posicion 0, posicion 1 * posicion 1 y asi
        //Esos valores multiplicados se guardan en una lista
        //Con los valores de esa lista se multiplican por los dias del mes que corresponda, siendo la posicion 0 enero
        //4.- Por ultimo esa lista de valores, se divide cada uno de ellos por 3,6
        //Esos valores divididos son los que corresponden a cada una de las G de cada mes
        //Por lo tanto esa lista se decuelve en este método
        //Para probar que funciona el metodo:
        GMeses = new ArrayList<>();
        double valor = ((5.5 * 1.39) * 31) / 3.6;
        GMeses.add(valor);
        valor = ((8.8 * 1.3) * 28) / 3.6;
        GMeses.add(valor);
        valor = ((13.9 * 1.19) * 31) / 3.6;
        GMeses.add(valor);
        valor = ((17.2 * 1.08) * 30) / 3.6;
        GMeses.add(valor);
        valor = ((19.9 * 1) * 31) / 3.6;
        GMeses.add(valor);
        valor = ((22.6 * 0.97) * 30) / 3.6;
        GMeses.add(valor);
        valor = ((25.1 * 1) * 31) / 3.6;
        GMeses.add(valor);
        valor = ((23 * 1.09) * 31) / 3.6;
        GMeses.add(valor);
        valor = (((18.3 * 1.23) * 30) / 3.6);
        GMeses.add(valor);
        valor = (((11.2 * 1.4) * 31) / 3.6);
        GMeses.add(valor);
        valor = (((6.9 * 1.51) * 30) / 3.6);
        GMeses.add(valor);
        valor = (((4.2 * 1.48) * 31) / 3.6);
        GMeses.add(valor);
        return GMeses;
    }

    public static List<Double> calculaTemp(String provincia) {
        //Para calcular la temperatura es necesario obtener los datos también de la base de datos
        //Hace falta la provincia
        //Con ello se consulta y se devuelve una lista de valores numéricos ordenados por mes

        //Para probar que funciona el metodo
        List<Double> tempMeses = new ArrayList<>();
        tempMeses.add(4.00);
        tempMeses.add(6.00);
        tempMeses.add(9.00);
        tempMeses.add(12.00);
        tempMeses.add(17.00);
        tempMeses.add(21.00);
        tempMeses.add(24.00);
        tempMeses.add(23.00);
        tempMeses.add(18.00);
        tempMeses.add(13.00);
        tempMeses.add(8.00);
        tempMeses.add(4.00);
        return tempMeses;
    }

    //Metodo para calcular las perdidas de los paneles por el polvo, angulo, etc...
    public static double calulaLDC(double LM, double LPS, double LAS, double Lohm, double LPN) {
        LM = (100 - LM) / 100;
        LPS = (100 - LPS) / 100;
        LAS = (100 - LAS) / 100;
        Lohm = (100 - Lohm) / 100;
        LPN = (100 - LPN) / 100;

        return LM * LPS * LAS * Lohm * LPN;
    }

    //Calculo para Rend,SPMP, Rendimiento en punto de max.potencia
    //Necesario para la clase PotEntradaInversor
    //Recibe el porcentaje del de las perdidas
    public static double perdidasLSPMP(double perdida) {
        double resto = 100 - perdida;
        resto = resto / 100;
        return 1 - resto;
    }

    //Metodo que calcula las perdidas generadas al cambiar corriente alterna a corriente continua
    public static double calculaPerdidasConversion(double perdida) {
        return (100 - perdida) / 100;
    }

    public static double perdidasLohm(double perdida) {
        double resto = 100 - perdida;
        resto = resto / 100;
        return 1 - resto;
    }

    public static double perdidasRed(double sombras, double otras) {
        double restoSombras = 100 - sombras;
        double restoOtras = 100 - otras;
        double restoTotal = (restoSombras / 100) * (restoOtras / 100);
        return 1 - restoTotal;
    }

    public static List<Double> calculoRadiacionesVentasEnergia(List<Double> listaGs) {
        List<Double> radiaciones = new ArrayList<>();
        double valor;
        for (int i = 0; i < 12; i++) {
            valor = listaGs.get(i) / diasMes(i);
            radiaciones.add(valor);
        }
        return radiaciones;
    }

    public static List<Double> calculoProduccionVentasEnergia(List<Double> radiaciones, List<Double> performanceRatios, double potInstalada) {
        List<Double> producciones = new ArrayList<>();
        double valor;
        for (int i = 0; i < 12; i++) {
            valor = diasMes(i) * radiaciones.get(i) * (performanceRatios.get(i) / 100) * potInstalada;
            producciones.add(valor);
        }
        return producciones;
    }

    public static int diasMes(int numeroMes) {
        if (numeroMes < 0 || numeroMes > 11) {
            return 30;
        } else {
            switch (numeroMes) {
                case 0:
                    return 31;
                case 1:
                    return 28;
                case 2:
                    return 31;
                case 3:
                    return 30;
                case 4:
                    return 31;
                case 5:
                    return 30;
                case 6:
                    return 31;
                case 7:
                    return 31;
                case 8:
                    return 30;
                case 9:
                    return 31;
                case 10:
                    return 30;
                case 11:
                    return 31;
                default:
                    return 30;
            }
        }
    }

    public static int diasMes(String nombreMes) {
        nombreMes = nombreMes.toLowerCase();
        switch (nombreMes) {
            case "enero":
                return 31;
            case "febrero":
                return 28;
            case "marzo":
                return 31;
            case "abril":
                return 30;
            case "mayo":
                return 31;
            case "junio":
                return 30;
            case "julio":
                return 31;
            case "agosto":
                return 31;
            case "septiembre":
                return 30;
            case "octubre":
                return 31;
            case "noviembre":
                return 30;
            case "diciembre":
                return 31;
            default:
                return 30;
        }
    }
}
