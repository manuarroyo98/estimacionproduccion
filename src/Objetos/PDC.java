package Objetos;

import java.util.ArrayList;

/**
 *
 * @author Adrián Sánchez
 */
//Clase para el objeto Potencia de maxima potencia del generador segun las perdidas 
public class PDC {

    //Objeto para obtener las potencias de maxima potencia del generado necesario para calcular los valores de PDC
    private PotMaxPotencia PM;
    //Variable para almacenar las perdidas
    private double LDC;
    //Array de los valores finales
    private ArrayList<Double> valoresPDC;

    public PDC(PotMaxPotencia PM) {
        this.PM = PM;
        this.LDC = Calculos.calulaLDC(3, 3, 3, 1, 3);
        this.valoresPDC = new ArrayList<>();
        double valor;
        for (int i = 0; i < 12; i++) 
        {
            //Añado los valores al array siguiendo la formula
            valor = PM.getValoresPM().get(i) * this.LDC;
            this.valoresPDC.add(valor);
        }
    }

    public PotMaxPotencia getPM() {
        return PM;
    }

    public void setPM(PotMaxPotencia PM) {
        this.PM = PM;
    }

    public double getLDC() {
        return LDC;
    }

    public void setLDC(double LDC) {
        this.LDC = LDC;
    }

    public ArrayList<Double> getValoresPDC() {
        return valoresPDC;
    }

    public void setValoresPDC(ArrayList<Double> valoresPDC) {
        this.valoresPDC = valoresPDC;
    }

}
