/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Manuel Arroyo Setien
 */
public class PotEntregadaRed {
    //Esta clase indica la potencia entregada a la red y sus perdidas
    //Su formula de calculo es PerdidasCableado * (1-perdidasRed)
    //Necesario una por cada mes
    
    private List<Double> potenciasRed;
    private double perdidasSombras, perdidasOtras;
    
    public PotEntregadaRed(PerdidasCableado perdCable, double perSombras, double perOtras) {
        potenciasRed = new ArrayList<>();
        this.perdidasSombras = perSombras;
        this.perdidasOtras = perOtras;
        rellenarLista(perdCable);
    }
    public List<Double> getPotenciasRed() {
        return potenciasRed;
    }

    public void setPotenciasRed(List<Double> potenciasRed) {
        this.potenciasRed = potenciasRed;
    }

    public double getPerdidasSombras() {
        return perdidasSombras;
    }

    public void setPerdidasSombras(double perdidasSombras) {
        this.perdidasSombras = perdidasSombras;
    }

    public double getPerdidasOtras() {
        return perdidasOtras;
    }

    public void setPerdidasOtras(double perdidasOtras) {
        this.perdidasOtras = perdidasOtras;
    }
    
    private void rellenarLista(PerdidasCableado p) {
        double valor;
        for (int i=0; i<p.getListaPerdidas().size(); i++) {
            valor = p.getListaPerdidas().get(i) * (1-Calculos.perdidasRed(perdidasSombras, perdidasOtras));
            this.potenciasRed.add(valor);
        }
    }
}
