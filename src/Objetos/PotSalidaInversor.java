package Objetos;

import java.util.ArrayList;

/**
 *
 * @author Adrián Sánchez
 */
//Clase para el objeto Potencia disponible a la salida del inversor (PAC)
public class PotSalidaInversor 
{
    //Objeto para obtener la potencia de entrada al inversor necesario para calcular la potencia de salida
    private PotEntradaInversor potEntradaInversor;
    //Variable para almacenar las perdidas generadas al hacer la conversion de corriente alterna a continua
    private double perdidasConversion;
    //Array para almacenar los valores finales de la Potencia
    private ArrayList<Double> valoresPotSalida;

    public PotSalidaInversor(PotEntradaInversor potEntradaInversor) {
        this.potEntradaInversor = potEntradaInversor;
        this.perdidasConversion = Calculos.calculaPerdidasConversion(10);
        this.valoresPotSalida = new ArrayList<>();
        double valor;
        for (int i = 0; i < 12; i++) 
        {
            valor = potEntradaInversor.getPotenciasEntrada().get(i) * this.perdidasConversion;
            //Añado los valores al array siguiendo la formula
            this.valoresPotSalida.add(valor);
        }
    }

    public PotEntradaInversor getPotEntradaInversor() {
        return potEntradaInversor;
    }

    public void setPotEntradaInversor(PotEntradaInversor potEntradaInversor) {
        this.potEntradaInversor = potEntradaInversor;
    }

    public double getPerdidasConversion() {
        return perdidasConversion;
    }

    public void setPerdidasConversion(double perdidasConversion) {
        this.perdidasConversion = perdidasConversion;
    }

    public ArrayList<Double> getValoresPotSalida() {
        return valoresPotSalida;
    }

    public void setValoresPotSalida(ArrayList<Double> valoresPotSalida) {
        this.valoresPotSalida = valoresPotSalida;
    }
    
    
}
